# Socially Driven Transition to Sustanaible Food System

Fork of old version of DRITF2SELEC to use as base for Austin project with Iamecon

Not yet a package but to render the file:
`firstExploration.Rmd`

From R:
```r
library(rmarkdown)
render(input="firstExploration.Rmd")
```


This repository centralise the first code and figure used to explore the factor driven adoption of Plant Based Alternative.

Most of this work as been made possible by funding from the [C2D3](https://www.c2d3.cam.ac.uk/) through the 2023 Early Career Research Seed Funding

https://www.c2d3.cam.ac.uk/opportunities/c2d3-early-career-researcher-seed-funding-2023


It brings together multiple partners

- https://fsi.org/
- https://iamecon.com/
- https://www.plantbasedfoods.org/
