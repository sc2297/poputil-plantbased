/*
neutralModel <- function(N,mu,tstep){
    pop=matrix(nrow=N,ncol=tstep) #initialise an empty matrix to store all populationo
    pop[,1]=1:N #initialise the first generation
    max_id=N    #store the max id to generate unique one. Can be removed but at the price f looking the max of id of the population at each time step. could be a bit slower when N >>>
    for(t in 2:tstep){
        rnd=runif(N)<mu #get the list of individual who will generate new traits 
        newtraits=sum(rnd)
        if(newtraits>0){
            new_max_id=max_id+newtraits 
            pop[rnd,t]=(max_id+1):new_max_id
            max_id=new_max_id
        }
        pop[!rnd,t]=sample(pop[,t-1],sum(!rnd),replace=T)
    }
    pop
}
*/

#include "stdio.h"
#include "stdlib.h"
//#include "include/population.h"

//much more  random than time(0)
unsigned long long rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((unsigned long long)hi << 32) | lo;
}

int main(int argc, char **argv)
{
	srand(rdtsc());   // Initialization, should only be called once.

	int N,tstep;
	float mu;
	sscanf(argv[1],"%i",&N); //get first argument as a number
	sscanf(argv[2],"%f",&mu); //get first argument as a number
	sscanf(argv[3],"%i",&tstep); //get first argument as a number
    int max_id=N;

    int * pop=(int *)malloc(N*tstep*sizeof(int));
    for(int i=0; i<N; i++){
        //pop[i]=(int *)malloc(tstep*sizeof(int));
        *(pop + i*tstep)=i;
    }

    for(int t=1; t<tstep; t++){
        for(int i=0; i<N; i++){
            float rnd;
            rnd=(float)(rand())/(float)(RAND_MAX);
            if(rnd<mu){
                max_id++;
                //printf("t=%i,mutate %i with newid %d\n",t,i,max_id);
                //pop[i][t]=max_id;
                *(pop+i*tstep+t)=max_id;
            }
            else{
                int newid;
                newid=rand()%N; //randomly select a new person
                //printf("t=%i,copy %i with id %d\n",t,newid,pop[newid][t-1]);
                //pop[i][t]=pop[newid][t-1];
                //pop[i][t]=pop[newid][t-1];
                *(pop+i*tstep+t)=*(pop+newid*tstep+(t-1));
            }
        }
    }
    return 0;

}


/**
    int ** pop=(int **)malloc(N*sizeof(int*));
    for(int i=0; i<N; i++){
        pop[i]=(int *)malloc(tstep*sizeof(int));
        pop[i][0]=i;
    }

    for(int t=1; t<tstep; t++){
        for(int i=0; i<N; i++){
            float rnd;
            rnd=(float)(rand())/(float)(RAND_MAX);
            if(rnd<mu){
                max_id++;
                //printf("t=%i,mutate %i with newid %d\n",t,i,max_id);
                pop[i][t]=max_id;
            }
            else{
                int newid;
                newid=rand()%N; //randomly select a new person
                //printf("t=%i,copy %i with id %d\n",t,newid,pop[newid][t-1]);
                pop[i][t]=pop[newid][t-1];
            }
        }
    }

    **/
