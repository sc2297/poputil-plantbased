source("../R/functions.R")
    #legend("center",legend=round(u,2),lty=1,col=cols,title="u",lwd=2)
library(scales)
m=5
tstep=300
p0=rep(1/m,m)  
sdf=T
bf=T
mus=c(.01,.001,.0001)
sdes=c(0.001,0.1,1)
betas=rev(c(-5,-1,0,1))
Js=c(-1.2,-.5,0,.5,1.2)
Ns=c(100,500,5000)
Us=list(lin=seq(0,1,length.out = 50),exp=10^seq(-2,1,length.out = 50))
Us=list(lin="runif",exp="rexp")
for(nu in names(Us)){
    errolist=list()
    turnov=list()
    u0=Us[[nu]]
    for(N in Ns){
        for(mu in mus){
            tau=4/mu
            #for(J in c(-1.2,-1,-.5,0,.5,1,1.2)){
            for(J in Js){
                pdf(paste0("noisevariances_",nu,"_J",J,"_N",N,"_mu",mu,".pdf"),width=13,height=12)
                par(mfrow=c(4,3),mar=c(1,2,0,0),oma=c(5,4,4,4),xpd=NA)
                for(beta in betas){
                    for(sde in sdes){
                        res=c()
                        #print(sde)
                        all=model(p0=p0,J=J,u0=u0,beta=10^(beta),sde=sde,tstep=tau+tstep,mu=mu,N=N,m=m)
                        res=(all[["freq"]][,tau:(tau+tstep)])/N
                        disappeared=rowSums(res)>0 #traits that disappeared
                        res=res[disappeared,,drop=F]
                        us=all[["ut"]][disappeared]
                        cols=rev(colorRampPalette(c("blue","yellow","red"))(length(us)))
                        names(cols)=sort(us)
                        plot(1,1,type="n",ylim=range(res,finite=T),xlim=c(0,tstep),xaxt="n",ylab="",xlab="")
                        #plot(1,1,type="n",ylim=c(0,.8),xlim=c(0,tstep))
                        if(bf)  mtext("% of pop",2,3,cex=1)
                        bf=F
                        tryCatch({
                        for(l in 1:length(us))
                            lines(res[l,],lwd=.5,col=alpha(cols[as.character(us[l])],.6))
                        },error=function(e)print(res))
                        #  mtext(bquote(beta==10^.(beta) * "," * J * ":" * .(J) * ",sd(" * epsilon * "):" * .(sde)),3,0,cex=.7)
                        if(sdf) mtext(bquote("sd(" * epsilon * ")" == .(sde)),3,1,cex=1)
                        #if(sdf) mtext(bquote("sd(" * epsilon * ")" == .(sde)),3,1,cex=1)
                        ft=res
                        ft=ft[rowSums(ft)>0,,drop=F]
                        ft=round(ft*N)
                        cumrow=rowSums(ft)
                        #print(cumrow)
                        errolist[[as.character(N)]][[as.character(mu)]][[as.character(J)]][[as.character(beta)]][[as.character(sde)]]=cumrow
                        Y=30
                        turnov[[as.character(N)]][[as.character(mu)]][[as.character(J)]][[as.character(beta)]][[as.character(sde)]]=sapply(1:(ncol(all$freq)-1),function(t)
                                                                                                                                           {
                                                                                                                                               rk1=rank(all$freq[,t],ties.method="min")
                                                                                                                                               rk2=rank(all$freq[,t+1],ties.method="min")
                                                                                                                                               r1=which(rk1>(nrow(all$freq)-Y))
                                                                                                                                               r2=which(rk2>(nrow(all$freq)-Y))
                                                                                                                                               return(sum(!(r2 %in% r1)))
                                                                                                                                           })
                    }
                    bf=T
                    sdf=F
                    mtext(bquote( beta == 10^.(beta)),4,1,cex=1)

                }
                axis(1)
                mtext("Time",1,3)
                #mtext(),3,1,cex=1,outer=T)
                #  e=sapply(c(0.001,.01,0.1,1,10),function(sde)as.expression(bquote("sd(" * epsilon * ")" == .(sde))))
                #    mtext(e,side=3,line=1,at=seq(0.1,.9,length.out=5),cex=1,outer=T)
                #legend("center",legend=round(u,2),lty=1,col=cols,title="u",lwd=2)
                dev.off()
                sdf=T
            }
        }
    }

    for( N in as.character(Ns)){
        pdf(paste0("CCFD_",nu,"_N",N,".pdf"),width=13,height=12)
        par(mfrow=c(length(Js),length(betas)),mar=c(1,2,0,0),oma=c(5,4,4,4),xpd=F)
        mcols=heat.colors(length(mus))
        names(mcols)=as.character(mus)
        for( J in as.character(Js)){
            for( beta in as.character(betas)){
                plot(1,1,log="xy",ylim=c(.01,100),xlim=c(1,1000000),type="n",xaxt="n",ylab="",xlab="")
                for( mu in as.character(mus)){
                    for( is in seq_along(sdes)){
                        sdec=as.character(sdes[is])
                        pointsCCFD(errolist[[N]][[mu]][[J]][[beta]][[sdec]],lty=is,type="l",col=mcols[mu])
                    }
                }
                mtext(bquote( beta == 10^.(beta)),3,1,cex=1)
            }
            mtext(bquote( J == .(J)),4,1,cex=1)
        }
        axis(1)
        mtext("frequency",1,3)
        mtext(paste("CCFD, N=",N),1,3,outer=T)
        dev.off()
    }

    for( N in as.character(Ns)){
        pdf(paste0("turnov_",nu,"_N",N,".pdf"),width=13,height=12)
        par(mfrow=c(length(Js),length(betas)),mar=c(1,2,0,0),oma=c(5,4,4,4),xpd=F)
        mcols=heat.colors(length(mus))
        names(mcols)=as.character(mus)
        for( J in as.character(Js)){
            for( beta in as.character(betas)){
                plot(1,1,ylim=c(0,Y),xlim=c(1,tstep),type="n",xaxt="n",ylab="",xlab="")
                for( mu in as.character(mus)){
                    for( is in seq_along(sdes)){
                        sdec=as.character(sdes[is])
                        lines(turnov[[N]][[mu]][[J]][[beta]][[sdec]],lty=is,type="l",col=mcols[mu])
                    }
                }
                mtext(bquote( beta == 10^.(beta)),3,1,cex=1)
            }
            mtext(bquote( J == .(J)),4,1,cex=1)
        }
        axis(1)
        mtext("Time",1,3)
        mtext(paste("Turnover top 20, N=",N),1,3,outer=T)
        dev.off()
    }
}

pdf("legend.pdf")
plot(1,1,type="n",ann=F,axes=F)
legend("center",lty=c(rep(1,3),seq_along(sdes)),col=c(mcols,rep(1,3)),legend=c(paste("mu=",mus),paste("sd_e=",sdes)),cex=4,lwd=8)
dev.off()

