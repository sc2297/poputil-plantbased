source("../R/functions.R")

LBK=readRDS("data/ceramics_lbk_merzbach.RDS")
simu=readRDS("artificialmodels/backup_artificalruns.RDS")

allfrequencies=lapply(simu$runs,"[[","freq")
allfrequencies[["LBK"]]=LBK

cols=RColorBrewer::brewer.pal(length(simu$runs),"Set2")
names(cols)=names(simu$runs)
cols[["LBK"]]="grey"


metrics=c("Simpson Diversity"=d.sim ,
"Disparity"=d.gap ,
"Turnover"=d.turn,
"Unique Variants"=d.unique ,
"Gini Index"=d.gini ,
"LogNorm Mean"=d.lognormmean ,
"LogNorm S.D."=d.lognormsd )


#For all scenario and LBK we compute all metrics (can be relatively long )
allmetrics=lapply(metrics,function(m)lapply(allfrequencies,function(f)m(f)))

limmetrics=lapply(allmetrics,lapply,range)
limmetrics=lapply(limmetrics,range)
for( i in names(metrics)){
    png(paste0("metrics_",gsub("[\\W \\.]","",i),".png"),pointsize=18)
    par(mar=c(2,4,1,1))
    plot(1,1,ylim=limmetrics[[i]],xlim=c(0.5,8.5),type="n",ylab=i,xlab="phase",main="",xaxt="n")
    for(m in names(allfrequencies)){
        mes=allmetrics[[i]][[m]]
        lines(seq(1,8,length.out=length(mes)),mes,col=cols[[m]],lwd=5)
   }
    
    axis(1,at=1:8,label=as.roman(7:14))
    dev.off()
}

#The same but this time we subsample the frequencies
allssmple=lapply(lapply(simu$runs,subsample,model=LBK),"[[","freq")
allssmple[["LBK"]]=LBK
allssmetrics=lapply(metrics,function(m)lapply(allssmple,function(f)m(f)))

limmetrics=lapply(allssmetrics,lapply,range)
limmetrics=lapply(limmetrics,range)
for( i in names(metrics)){
    png(paste0("metrics_",gsub("[\\W \\.]","",i),"_subsample.png"),pointsize=18)

    par(mar=c(2,4,1,1))
    plot(1,1,ylim=limmetrics[[i]],xlim=c(0.5,8.5),type="n",ylab=i,xlab="",main="",xaxt="n")
    for(m in names(allfrequencies)){
        mes=allssmetrics[[i]][[m]]
        lines(seq(1,8,length.out=length(mes)),mes,col=cols[[m]],lwd=5)
    }
    axis(1,at=1:8,label=as.roman(7:14),las=3)
    dev.off()
}

