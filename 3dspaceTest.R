
#Just to add small but (maybe) important note about interpolation.

#Using very nice package "akima" you can easily interpolate your data:

library(akima)
library(rgl)
library(deldir)
#install.packages("deldir")



n_interpolation <- 10
finalUtility=finalUtility[finalUtility$beta<.10,]
x=finalUtility[,1]
y=finalUtility[,2]
z=finalUtility[,3]
z=z-min(z)
y=y[z>0]
x=x[z>0]
z=z[z>0]
zlim <- range(z)
zlen <- zlim[2] - zlim[1] + 1
colorlut <- terrain.colors(zlen)
col <- colorlut[ z - zlim[1] + 1 ] 
open3d()
surface3d(x, y, z, color = cols, back = "filled")


# # Triangulate it in x and y
del <- deldir(x[1:10], y[1:10], z = z[1:10])
del <- deldir(x, y, z = z)
triangs <- do.call(rbind, triang.list(del))
# 
# # Plot the resulting surface
plot3d(x, y, z, type = "n")
png("ftre.png")
triangles3d(triangs[, c("x", "y", "z")], color = cols,back="filled",lwd=0,size=0)
#surface3d(triangs[, "x"],triangs[, "y"],triangs[, "z"], color = cols,back="filled")
rgl.snapshot("test.png")
dev.off()




spline_interpolated <- interp(x, y, z, xo=seq(min(x), max(x), length = n_interpolation),
                              yo=seq(min(y), max(y), length = n_interpolation),
                              linear = FALSE, extrap = TRUE)

x.si <- spline_interpolated$x
y.si <- spline_interpolated$y
z.si <- spline_interpolated$z

persp3d(x.si, y.si, z.si, col = "gray")
rglwidget()
